﻿using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Support.UI;
using SportsTracker.Tests.Skeleton.Utils;

namespace SportsTracker.Tests.Skeleton.Tests
{
    public sealed class SportsTrackerTest
    {
        private const string AppiumServerUrl = "http://127.0.0.1:4723/wd/hub";
        
        private AndroidDriver<AndroidElement> Driver { get; set; }

        [TearDown]
        public void TearDown()
        {
            Driver.Quit();
        }

        [Test]
        public void Test1_UserCanRecordWorkout()
        {
            // TODO: Implement following steps:

            // 1. Enable location
            // 2. Launch Sports Tracker
            // 3. Read workout path from .csv file (file added to Resources)
            // 4. Set initial location (initialize LocationSimulator and set first location in the path)
            // 5. Click Start button on Home view
            // 6. Click Continue button on NewActivity view
            // 7. Click Start button on RecordActivity view
            // 8. Simulate location changes (use LocationSimulator class)
            // 9. Click Stop button on RecordActivity view and then click End button
            // 10. Click Save button on SaveActivity view
        }

        [Test]
        public void Test2_UserIsInformedAboutDisabledLocationOnWorkoutStart()
        {
            // TODO: Implement following steps:

            // 1. Disable location
            // 2. Launch Sports Tracker
            // 3. Click Start button on Home view
            // 4. Read displayed message and assert that displayed message is: "GPS is disabled! Would you like to enable it?"
        }

        private void DisableLocation()
        {
            // TODO: Implement following steps:

            // 1. Launch LocationSettings by using ApplicationLauncher
            // 2. Disable location by using page object class LocationSettingsView
            // 3. Quit LocationSettings
        }

        private void EnableLocation()
        {
            // TODO: Implement following steps:

            // 1. Launch LocationSettings by using ApplicationLauncher
            // 2. Enable location by using page object class LocationSettingsView
            // 3. Quit LocationSettings
        }

        private void LaunchSportsTracker()
        {
            // TODO: Initialize Driver by using ApplicationLauncher class
        }
    }
}
