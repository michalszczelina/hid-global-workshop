﻿using OpenQA.Selenium.Appium.Android;

namespace SportsTracker.Tests.Skeleton.ApplicationViews
{
    public abstract class BasePageObject
    {
        protected readonly AndroidDriver<AndroidElement> Driver;

        public BasePageObject(AndroidDriver<AndroidElement> driver)
        {
            Driver = driver;
        }
    }
}
