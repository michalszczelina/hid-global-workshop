﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Support.UI;

namespace SportsTracker.Tests.Skeleton.ApplicationViews.SportsTracker
{
    public sealed class HomeView : BasePageObject
    {
        // TODO: Find Id of StartButton element
        private readonly By _startButtonSelector = By.Id("");

        public HomeView(AndroidDriver<AndroidElement> driver) : base(driver)
        {
        }

        // TODO: Implement finding StartButton by _startButtonSelector
        private AndroidElement StartButton => throw new NotImplementedException();

        public void ClickStartButton()
        {
            StartButton.Click();
        }

        public void WaitUntilViewDisplayed()
        {
            // TODO: Implement wait for StartButton to be visible
        }
    }
}
