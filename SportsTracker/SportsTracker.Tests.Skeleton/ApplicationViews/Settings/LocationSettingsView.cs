﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;

namespace SportsTracker.Tests.Skeleton.ApplicationViews.Settings
{
    public sealed class LocationSettingsView : BasePageObject
    {
        public LocationSettingsView(AndroidDriver<AndroidElement> driver) : base(driver)
        {
        }

        // TODO: Add LocationSwitch selector
        private AndroidElement LocationSwitch => throw new NotImplementedException();

        // TODO: Check if attribute "Checked" on LocationSwitch is "true"
        private bool LocationEnabled => throw new NotImplementedException();

        public void SwitchLocationOff()
        {
            // TODO: If location enabled, click LocationSwitch
        }

        public void SwitchLocationOn()
        {
            // TODO: If location disabled, click LocationSwitch
        }
    }
}
