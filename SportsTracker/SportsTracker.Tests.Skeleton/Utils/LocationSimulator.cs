﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;

namespace SportsTracker.Tests.Skeleton.Utils
{
    public sealed class LocationSimulator
    {
        private readonly AndroidDriver<AndroidElement> _driver;

        public LocationSimulator(AndroidDriver<AndroidElement> driver)
        {
            _driver = driver;
        }

        public void FollowPath(IEnumerable<Location> path, TimeSpan interval)
        {
            // TODO: Iterate through path, set location on each iteration and wait given time interval after each iteration
        }

        public void SetLocation(Location location)
        {
            // TODO: Set location on driver
        }        
    }
}
