﻿using System.Collections.Generic;
using OpenQA.Selenium.Appium;

namespace SportsTracker.Tests.Skeleton.Utils
{
    public static class LocationCsvReader
    {
        public static IEnumerable<Location> ReadWorkoutPathFromResourceCsvFile(string csvFileContent)
        {
            var lines = csvFileContent.TrimEnd().Split("\r\n");

            foreach (string line in lines)
            {
                var coordinates = line.Split(";");

                yield return new Location
                {
                    Latitude = double.Parse(coordinates[0]),
                    Longitude = double.Parse(coordinates[1])
                };
            }
        }
    }
}
