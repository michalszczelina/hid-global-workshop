﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using SportsTracker.Tests.Skeleton.ApplicationViews.SportsTracker;

namespace SportsTracker.Tests.Skeleton.Utils
{
    public static class ApplicationLauncher
    {
        private const string SportsTrackerAppPackage = @"com.stt.android";
        private const string SportsTrackerAppActivity = @"com.stt.android.launcher.ProxyActivity";
        private const string LocationSettingsAppPackage = "com.android.settings";
        private const string LocationSettingsAppActivity = "com.android.settings.Settings$LocationSettingsActivity";

        public static AndroidDriver<AndroidElement> LaunchSportsTracker(string appiumServerUri)
        {
            AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(
                new Uri(appiumServerUri),
                GetAppiumOptions(SportsTrackerAppPackage, SportsTrackerAppActivity, "Android"));

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            new HomeView(driver).WaitUntilViewDisplayed();

            return driver;
        }

        public static AndroidDriver<AndroidElement> LaunchLocationSettings(string appiumServerUri)
        {
            AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(
                new Uri(appiumServerUri),
                GetAppiumOptions(LocationSettingsAppPackage, LocationSettingsAppActivity, "Android"));

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            return driver;
        }

        private static AppiumOptions GetAppiumOptions(string appPackage, string appActivity, string platformName)
        {
            var options = new AppiumOptions();

            options.AddAdditionalCapability("appPackage", appPackage);
            options.AddAdditionalCapability("appActivity", appActivity);
            options.AddAdditionalCapability(MobileCapabilityType.PlatformName, platformName);
            options.AddAdditionalCapability("autoGrantPermissions", true);

            return options;
        }
    }
}
