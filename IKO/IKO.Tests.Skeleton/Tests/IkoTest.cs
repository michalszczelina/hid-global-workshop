﻿using IKO.Tests.Skeleton.ApplicationViews;
using IKO.Tests.Skeleton.Utils;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;

namespace IKO.Tests.Skeleton.Tests
{
    public sealed class IkoTest : BaseTest
    {
        [Test]
        public void Test1_UserCanLoginWithValidPinNumber()
        {
            // TODO: Implement following steps:

            // 1. Click App Demo button on the first screen
            // 2. Click Login button on the second screen
            // 3. Enter valid PIN number (4 digits)
            // 4. Skip intro
            // 5. Expand User Profile view
            // 6. Read name of logged user
            // 7. Assert that user name is 'Jan Iko'
        }

        [Test]
        public void Test2_UserCannotLoginWithInvalidPinNumber()
        {
            // TODO: Implement following steps:

            // 1. Click App Demo button on the first screen
            // 2. Click Login button on the second screen
            // 3. Enter invalid PIN number (3 digits)
            // 4. Assert that EnterPin view is still displayed
        }

        [Test]
        public void Test3_UserCanLogoutFromApplication()
        {
            // TODO: Implement following steps:

            // 1. Login to the app (the same steps as in valid login test, the steps can be also reused by moving the code to separate method)
            // 2. Expand User Profile view
            // 3. Click Logout button
            // 4. Confirm logout
            // 5. Assert that the second view is displayed (the view with Login button)
        }

        [Test]
        public void Test4_UserCanTransferMoneyFromUsdAccount()
        {
            // TODO: Implement following steps:

            // 1. Login to the app
            // 2. Swipe account to display account 'PKO KONTO ZA ZERO USD'
            // 3. Click Transfer button on the account component
            // 4. Enter valid values into mandatory inputs in Transfer view (example valid account number: 10105000997603123456789123)
            // 5. Click Send button
            // 6. Assert Summary view
            // 7. Click Confirm button
            // 8. Enter valid PIN (4 digits)
            // 9. Assert that amount of transferred money on Success view is the same as entered in Transfer view
        }

        [Test]
        public void Test5_UserCanTransferMoneyToDefinedRecipient()
        {
            // TODO: Implement following steps:

            // 1. Login to the app
            // 2. Click Transfer button on the account component
            // 3. Click "Search for recipient" button
            // 4. Select recipient 'Ewa Iko' from the list
            // 5. Assert that mandatory inputs are filled with correct values
        }

        [Test]
        public void Test6_UserCannotTransferMoneyToInvalidAccountNumber()
        {
            // TODO: Implement following steps:

            // 1. Login to the app
            // 2. Click Transfer button on the account component
            // 3. Enter invalid account number into beneficiary's account number input
            // 4. Assert that validation error is displayed under the input
            // Expected validation error: "Enter a valid account number of the recipient"
        }

        [Test]
        public void Test7_UserCanHideAccountsComponent()
        {
            // TODO: Implement following steps:

            // 1. Login to the app
            // 2. Press and hold account component
            // 3. Move the component to hide area
            // 4. Assert that the component is not displayed
        }
    }
}
