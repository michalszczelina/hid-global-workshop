﻿using IKO.Tests.Skeleton.Utils;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Android;

namespace IKO.Tests.Skeleton.Tests
{
    public abstract class BaseTest
    {
        private const string AppiumServerUrl = "http://127.0.0.1:4723/wd/hub";
        private const string IkoApkPath = @"C:\AndroidApps\iko.apk";

        protected AndroidDriver<AndroidElement> Driver { get; set; }

        [SetUp]
        public void SetUp()
        {
            Driver = ApplicationLauncher.LaunchAndroidApp(IkoApkPath, AppiumServerUrl);
        }

        [TearDown]
        public void TearDown()
        {
            Driver.Quit();
        }
    }
}
