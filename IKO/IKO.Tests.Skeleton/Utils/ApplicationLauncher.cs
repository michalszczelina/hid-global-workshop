﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;

namespace IKO.Tests.Skeleton.Utils
{
    public static class ApplicationLauncher
    {
        public static AndroidDriver<AndroidElement> LaunchAndroidApp(string apkFilePath, string appiumServerUri)
        {
            AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(
                new Uri(appiumServerUri),
                GetAppiumOptions(apkFilePath, "Android"));

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

            return driver;
        }

        private static AppiumOptions GetAppiumOptions(string apkFilePath, string platformName)
        {
            var options = new AppiumOptions();

            options.AddAdditionalCapability(MobileCapabilityType.App, apkFilePath);
            options.AddAdditionalCapability(MobileCapabilityType.PlatformName, "Android");
            options.AddAdditionalCapability("autoGrantPermissions", true);

            return options;
        }
    }
}
