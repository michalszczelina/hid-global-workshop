﻿using OpenQA.Selenium.Appium.Android;

namespace IKO.Tests.Skeleton.ApplicationViews
{
    public sealed class IntroView : BasePageObject
    {
        public IntroView(AndroidDriver<AndroidElement> driver) : base(driver)
        {
        }

        public void SkipIntro()
        {
            WaitForIntroToBeDisplayed();
            // TODO: Click Back button
        }

        private void WaitForIntroToBeDisplayed()
        {
            // TODO: Implement wait here (wait for visibility of some IntroView element)
        }
    }
}
