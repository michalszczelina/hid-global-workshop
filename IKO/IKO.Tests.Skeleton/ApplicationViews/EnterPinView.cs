﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Interactions;

namespace IKO.Tests.Skeleton.ApplicationViews
{
    public sealed class EnterPinView : BasePageObject
    {
        public EnterPinView(AndroidDriver<AndroidElement> driver) : base(driver)
        {
        }

        // TODO: Find selector for root element, the element should be unique across the application so that if any element found by the selector,
        // we are sure that EnterPinView is displayed
        public bool IsDisplayed => Driver.FindElements().Count > 0;

        public IntroView EnterValidPin(string pin)
        {
            new Actions(Driver).SendKeys(pin).Perform();
            return new IntroView(Driver);
        }
    }
}
