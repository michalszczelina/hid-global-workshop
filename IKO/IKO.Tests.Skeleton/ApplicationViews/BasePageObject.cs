﻿using OpenQA.Selenium.Appium.Android;

namespace IKO.Tests.Skeleton.ApplicationViews
{
    public abstract class BasePageObject
    {
        protected readonly AndroidDriver<AndroidElement> Driver;

        internal BasePageObject(AndroidDriver<AndroidElement> driver)
        {
            Driver = driver;
        }
    }
}
