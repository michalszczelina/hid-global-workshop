﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;

namespace IKO.Tests.Skeleton.ApplicationViews
{
    public sealed class DemoView : BasePageObject
    {
        public DemoView(AndroidDriver<AndroidElement> driver) : base(driver)
        {
        }

        // TODO: Add LoginButton selector
        private AndroidElement LoginButton => Driver.FindElement();

        public EnterPinView ClickLoginButton()
        {
            LoginButton.Click();
            return new EnterPinView(Driver);
        }
    }
}
