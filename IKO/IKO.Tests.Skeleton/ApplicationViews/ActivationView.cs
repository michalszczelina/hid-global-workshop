﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;


namespace IKO.Tests.Skeleton.ApplicationViews
{
    public sealed class ActivationView : BasePageObject
    {
        public ActivationView(AndroidDriver<AndroidElement> driver) : base(driver)
        { 
        }

        // TODO: Add DemoButton selector
        private AndroidElement DemoButton => Driver.FindElement();

        public DemoView ClickSeeDemoButton()
        {
            DemoButton.Click();
            return new DemoView(Driver);
        }
    }
}
